<div align="center">

  # Scripts Linux Server

[![pipeline status](https://gitlab.com/ignisus/zeus/badges/main/pipeline.svg)](https://gitlab.com/ignisus/zeus/-/commits/main)

[![LINUX](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)](https://www.linux.org/pages/download/)
[![BASH](https://img.shields.io/badge/GNU%20Bash-4EAA25?style=for-the-badge&logo=GNU%20Bash&logoColor=white)](https://www.gnu.org/software/bash/manual/bash.html)

</div>
