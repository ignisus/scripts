#!/bin/bash

function set_java() {
    local java_path=$1
    sudo update-alternatives --set java $java_path
}

# Manage multiple terminals
function new_tab() {
    local title=$1
    local directory=$2
    local command="./start.sh; exec bash"

    # Check if tmux is available
    if command -v tmux >/dev/null 2>&1; then
        if [ -z "$TMUX" ]; then
            tmux new-session -d -s "$title" -c "$directory" "$command"
        else
            tmux new-window -n "$title" -c "$directory" "$command"
        fi
        echo -e "$title - DONE\n"
    elif command -v lxterminal >/dev/null 2>&1; then
        # Use lxterminal if tmux is not available
        lxterminal --title="$title" -e "bash -c 'cd $directory; $command'"
        echo -e "$title - DONE\n"
    else
        # No supported terminal found
        echo "Error: $title - Terminal not supported :C please install tmux, or lxterminal."
    fi
    sleep 1
}

# Minecraft Servers
set_java /usr/lib/jvm/bellsoft-java21-full-amd64/bin/java
new_tab "Proxy" "/mnt/ignisus/ignisusland/proxy"
sleep 1
new_tab "Limbo" "/mnt/ignisus/ignisusland/limbo"
sleep 1
new_tab "Lobby" "/mnt/ignisus/ignisusland/lobby"
sleep 1
new_tab "Vanilla" "/mnt/ignisus/ignisusland/vanilla"
