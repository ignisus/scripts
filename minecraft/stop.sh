#!/bin/bash

function stop_server() {
    local directory=$1
    local server_name=$2

    echo "Stopping $server_name server..."

    cd "$directory"

    # Attempt to gracefully shut down the server
    if tmux has-session -t "$server_name" 2>/dev/null; then
        echo "Sending stop command to $server_name via tmux..."
        tmux send-keys -t "$server_name" C-c
        tmux send-keys -t "$server_name" 'stop' Enter
        sleep 15  # Wait for server to shut down gracefully
        tmux kill-session -t "$server_name"
    fi

    # Force
    local pids=$(pgrep -f "$directory")
    if [ ! -z "$pids" ]; then
        echo "Force stopping any remaining processes..."
        kill -9 $pids
    fi

    echo "$server_name server stopped."
}

# Stop all Minecraft servers
stop_server "/mnt/ignisus/ignisusland/proxy" "Proxy"
stop_server "/mnt/ignisus/ignisusland/limbo" "Limbo"
stop_server "/mnt/ignisus/ignisusland/lobby" "Lobby"
stop_server "/mnt/ignisus/ignisusland/vanilla" "Vanilla"
