#!/bin/bash

# Ignisus Logo
printf "\033[0;33m      _,     ,_\n   .'/  ,_   \\'.\n"
printf "\033[0;91m  |  \\__( >__/  |\n"
printf "\033[0;31m  \\             /\n   '-..__ __..-'\n"
printf "\033[0;95m        /_\\ \n\033[0m"
printf "\033[0;33m      IGNISUS\033[0m\n" 

echo "======= INITIALIZING SYSTEM ======="

# Maintenance
sudo apt clean #--dry-run
sudo apt update
sudo apt install -y
sudo apt upgrade -y
sudo apt autoremove -y
sudo apt autoclean
echo -e "\n$(date "+%T") - UPDATE DONE\n"

# Firewall (nftables)
sudo nft -f /etc/nftables.conf
sudo nft list ruleset | grep -E 'dport'
echo -e "\n$(date "+%T") - FIREWALL DONE\n"

# Shield anti-DDOS
#echo "ddos restarting..."
#sudo systemctl restart ddos
#echo -e "\n$(date "+%T") - DDOS ON\n"

# Server SSH
sudo systemctl restart ssh
sudo systemctl status --no-pager ssh
echo -e "\n$(date "+%T") - SSH DONE\n"

# Email Server
sudo systemctl reload postfix

# WEB Servers
sudo systemctl stop apache2.service
sudo /opt/lampp/lampp stop
sudo /opt/lampp/lampp start
echo -e "\n$(date "+%T") - WEB DONE https://localhost/\n"

# Remote Desktop
# sudo systemctl restart xrdp 
# sudo systemctl status --no-pager xrdp
# sudo systemctl is-active xrdp
# echo -e "\n$(date "+%T") - REMOTE DESKTOP DONE\n"

# Minecraft Servers
./minecraft/start.sh